/*
   * PAPER PLANES GAME SOURCE CODE:
 * The aim of this game is to dodge the obstacles and gain
 * levels to reach a high score. The controls are WASD or 
 * the arrow keys.
 * Coded by: Tristan Bongolan
 */

//Global Variables
int width = 516;
int height = 768;
int x = width/2;
int y = 0;
int ySpeed = 5;
int xSpeed = 0;
int level = 0;
boolean shattered = false;
boolean pause = false;


void setup() {
  size(516, 768);
  background(230, 92, 0);
  keyPressed();
  smooth();
  createFont("Georgia", 20 );
  textSize(20);
}

void draw() {
  planeSpeed();
  backDrop();
  plane();
  setObstacles(level);
  collisionChecker(level);
  statusDisplay();
}

/*
    PLANE HANDLER AND PLANE GRAPHICS
 */

//Key press event handlers
void keyPressed() {
  if (!shattered && !pause) {
    if (key == 'a') {
      if (xSpeed == -4) {
        xSpeed = -4;
        ySpeed = 1;
      } else {
        xSpeed -= 2;
        if (xSpeed < 0) {
          ySpeed -= 2;
        } else {
          ySpeed += 2;
        }
      }
    }
    if (key == 'd') {
      if (xSpeed == 4) {
        xSpeed = 4;
        ySpeed = 1;
      } else {
        xSpeed += 2;
        if (xSpeed > 0) {
          ySpeed -= 2;
        } else {
          ySpeed += 2;
        }
      }
    }
  }

  //R for restart  
  if (key == 'r') {
    planeReset();
  }

  //P for pause
  if (key == 'p') {
    if (looping) {
      pause = true;
      text("PAUSED", (width/2) - 30, height/2);
      noLoop();
    } else {
      pause = false;
      loop();
    }
  }
}

void plane() {
  fill(255);
  if (ySpeed == 5 && xSpeed == 0) {
    triangle(x-10, y-10, x+10, y-10, x, y+10);
  }
  if (ySpeed == 3 && xSpeed == 2) {
    triangle(x, y-10, x-10, y+4, x+8, y+8);
  }
  if (ySpeed == 1 && xSpeed == 4) {
    triangle(x-10, y-10, x-10, y+10, x+10, y);
  }
  if (ySpeed == 3 && xSpeed == -2) {
    triangle(x, y-10, x+10, y+2, x-8, y+8);
  }
  if (ySpeed == 1 && xSpeed == -4) {
    triangle(x+10, y-10, x+10, y+10, x-10, y);
  }
  if (ySpeed == 0) {
    xSpeed = 0;
  }
}

//This function updates the position of the plane for each loop iteration
void planeSpeed() {
  x += xSpeed;
  y += ySpeed;
  if (y > height) {
    y = 0;
    level++;
  }
  if (x > width) {
    x = 0;
  } else if (x < 0) {
    x = width;
  }
}

void planeReset() {
  shattered = false;
  x = width/2;
  y = 0;
  ySpeed = 5;
  xSpeed = 0;
  level = 0;
  obstacleLocations = new int[2][20];
}

/*
    User interface/display
 */

void statusDisplay() {
  text(level, 460, 20);
  text("Level: ", 400, 20);
}

/*
  OBSTACLE HANDLERS
 */

int[][] obstacleLocations = new int[2][20]; //max of 20 obstacles

void setObstacles(int number) {
  if (number > 20) { 
    number = 20;
  }
  //if plane resets at the start
  if (y == 0) {
    //randomiser = (int)random(number);
    for (int i = 0; i < number; i++) {
      obstacleLocations[0][i] = (int)random((float)width); //<>//
      obstacleLocations[1][i] = (int)random((float)height/4, height); //<>//
    }
  }
  for (int i = 0; i < number; i++) { //<>//
    drawObstacles(obstacleLocations[0][i], obstacleLocations[1][i]);
  }
}

void drawObstacles(int xObs, int yObs) {
  ellipse((float)xObs, (float)yObs, 50, 50);
}

void collisionChecker(int number) {
  if (number > 20) {
     number = 20; 
  }
  for (int i = 0; i < number; i++) {
    if (dist(x, y, obstacleLocations[0][i], obstacleLocations[1][i]) <= 35) {
      Shatter();
      xSpeed = 0;
      ySpeed = 0;
      text("GAME OVER", 4*width/10, height/4);
      text("PRESS THE 'R' KEY TO RESTART", width/4, 3*height/8);
    }
  }
}
void Shatter() {
  shattered = true;
  triangle(x-4, y-4, x, y-4, x-2, y);
  triangle(x+12, y-8, x+16, y-4, x+12, y -4);
  triangle(x+8, y+8, x+10, y+14, x+6, y+12);
  triangle(x-8, y-10, x-16, y-4, x-8, y+4);
}

//BACKGROUND AND GRAPHICS
//Background of the game
void backDrop() {
  stroke(0, 0, 0);
  float brickHeight = height/42; 
  int brickLength = width/10;
  int windowHeight = 95;
  int windowLength = 150;
  for (int i = 0; i <= width; i += brickLength) {
    int l = 0;
    for (int k = 0; k <= height; k += brickHeight) {
      fill(230, 92, 0);
      strokeWeight(1);
      if (l%2 == 0) {
        rect(i, k, brickLength, brickHeight);
        l++;
      } else {
        rect(i - brickLength/2, k, brickLength, brickHeight);
        l++;
      }
    }
  }
  for (int i = 0; i <= height; i += height/4) {
    fill(0, 153, 255);
    strokeWeight(2);
    rect(width/2 - windowLength/2, i + 50, windowLength, windowHeight);
  }
}